﻿using AIStudio.Wpf.AvalonDockPrism.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace AIStudio.Wpf.AvalonDockPrism
{

    public class ModuleTestModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {

        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<TestView>();
            containerRegistry.RegisterForNavigation<Test2View>();
            containerRegistry.RegisterForNavigation<Test3View>();
        }
    }
}
