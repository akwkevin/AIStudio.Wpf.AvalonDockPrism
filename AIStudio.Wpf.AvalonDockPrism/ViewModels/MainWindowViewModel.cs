﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using AIStudio.Wpf.AvalonDockPrism.Views;
using Prism.Ioc;
using AIStudio.Wpf.AvalonDockPrism.Avalon;
using System.Windows.Threading;
using System;
using System.Windows.Controls;

namespace AIStudio.Wpf.AvalonDockPrism.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private readonly IRegionManager _regionManager;
        private readonly IContainerExtension _container;

        private string _title = "Prism Unity Application";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public DelegateCommand<string> NavigateCommand { get; private set; }

        public DelegateCommand<string> NavigateCommand2 { get; private set; }

        public DelegateCommand<string> NavigateCommand3 { get; private set; }


        public MainWindowViewModel(IRegionManager regionManager,  IContainerExtension container)
        {
            _regionManager = regionManager;
            _container = container;

            NavigateCommand = new DelegateCommand<string>(Navigate);
            NavigateCommand2 = new DelegateCommand<string>(Navigate2);
            NavigateCommand3 = new DelegateCommand<string>(Navigate3);
        }

        private void Navigate(string navigatePath)
        {
            if (navigatePath != null)
                _regionManager.RequestNavigate("ContentRegion", navigatePath);
        }

        private void Navigate2(string navigatePath)
        {
            if (navigatePath != null)
                _regionManager.RequestNavigate("ContentRegion2", navigatePath);
        }

        private void Navigate3(string navigatePath)
        {
            if (navigatePath != null)
                _regionManager.RequestNavigate("ContentRegion3", navigatePath);
        }
    }
}
